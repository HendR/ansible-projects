# create user role 
-  Create dir auth_keys in the same level  of create_user folder and add file under it with user name and put in it the public key of the user (do this for each user)
-  To make super users with NOPASSWD add users in 'admins' group
-  Define variables in playbook like this 
```sh
 vars:
    users:
      - name:  user_name
        groups:
          - secondery_group_of_user
          - admins
      - name: add_another_user
    key:
      - user_name
```